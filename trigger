CREATE TRIGGER Delelte_dishes
	ON [Dish] AFTER DELETE AS
		IF EXISTS (SELECT [order].ID FROM [Order] 
			JOIN deleted ON deleted.id = [Order].DishID)
BEGIN 
	DELETE FROM [ORDER]
		WHERE ID  =  (SELECT [order].ID FROM [Order] 
			JOIN deleted ON deleted.id = [Order].DishID)
END; 